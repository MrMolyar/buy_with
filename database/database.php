<?php
    function db_connect(){                                                                          //connect db
        $mysqli = new mysqli("localhost", "root", "", "db");
        $mysqli -> query("SET NAMES 'utf-8'");
        if (!$mysqli){
            echo "Не удаётся получить доступ к MySQL"; PHP_EOL;
            echo "Код ошибки: "; mysqli_connect_errno(); PHP_EOL;
            echo "Текст ошибки: "; mysqli_connect_error(); PHP_EOL;
            exit;
        }
        if(!mysqli_set_charset($mysqli, 'utf8')){
            printf("Ошибка при наборе символов utf-8 %s\n", mysqli_error($mysqli));
            exit;
        }
        return $mysqli;
    }

    function db_close($db){                                                                         //close db
        mysqli_close($db);
    }

    function select_order($table_name){                                                                       //select all order from db
        $db = db_connect();
        $result = mysqli_query($db, "SELECT * FROM `$table_name`");
        while ($row = mysqli_fetch_row($result)){
            $response[count($response)] = $row;
        }
        db_close($db);
        return $response;
    }

    function check_table($name_table){                                                             //check existence table
        $db = db_connect();
        $query = "SHOW TABLE LIKE `$name_table`";
        $res = mysqli_query($db, $query);
        if ($res) {
            db_close($db);
            return true;
        }
        else{
            db_close($db);
            return false;
        }
    }

    function db_add($array, $name_table, $s){                                                            //formation top 50 orders
        $db = db_connect();
        if (!check_table($name_table)) {
            mysqli_query($db, "CREATE TABLE `$name_table` (`id` int(11) PRIMARY KEY NOT NULL, `kod_item` varchar (10), `count` int(10), `buy_with` varchar(10))");
            for ($i = 0; $i < $s; $i++) {
                mysqli_query($db, "INSERT INTO `$name_table` (`id`, `kod_item`, `count`) VALUES ('{$i}', '{$array[$i][0]}', '{$array[$i][1]}')");
            }
            /*  else{
                  for ($i = 0; $i < 50; $i++) {
                      mysqli_query($db, "UPDATE `top_orders` SET `kod_item` = '{$array[$i][0]}', `count` = '{$array[$i][1]}' WHERE `id` = '{$i}'");
                  }
              }*/
            db_close($db);
        }
    }
  /*  function db_add_sort($array){
        $db = db_connect();
        if (!check_table(orders_sort)){
            mysqli_query($db, "CREATE TABLE `orders_sort` (`id` int(11) NOT NULL PRIMARY KEY, `item_kod` varchar (10), `count` int(10))");
            for ($i = 0; $i < count($array); $++){
                mysqli_query($db, "INSERT INTO `top_orders` (`id`, `kod_item`, `count`) VALUES ('{$i}', '{$array[$i][0]}', '{$array[$i][1]}')");
            }
        }
        db_close($db);
    }*/