<?php
/*function decode_json($json_string){                         //json decode
    $json_d = json_decode($json_string);
    return $json_d;
}*/

function find_item_kod($json_d){                            //find item kod
    $json_items = $json_d -> items;
    foreach ($json_items as $value){
        $result[] = $value -> item_kod;
    }
    return $result;
}

function find_item_count($json_d){
    $json_items = $json_d -> items;
    foreach ($json_items as $value){
        $result[] = $value -> item_count;
    }
    return $result;
}

function score($json_d, $array){                                                       //single score item
    $item_kod = find_item_kod($json_d);
    $item_count = find_item_count($json_d);
    $bool = false;
    for ($j = 0; $j < count($item_kod); $j++) {
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i][0] == $item_kod[$j]) {
                $array[$i][1] += $item_count[$j];
                $bool = true;
            }
        }
        if (!$bool) {
            $s = count($array);
            $array[$s][0] = $item_kod[$j];
            $array[$s][1] = $item_count[$j];
        }
    }
    return $array;
}

    function json_d_array($json_string){                                                        //array json_decode
    for ($i = 0; $i < count($json_string); $i++){
        $result[] = json_decode($json_string[$i][3]);
    }
    return $result;
}

function all_score($json_string){                                                                         //score all item
    $array[0][0] = NULL;
    $array[0][1] = NULL;
    $json_d_array = json_d_array($json_string);
    for ($i = 0; $i < count($json_string); $i++){
        $json_d = $json_d_array[$i];
        $array = score($json_d, $array);
    }
    return $array;
}

function sort_bubble($json_string){                                            //bubble sort
    $array = all_score($json_string);
    for ($i = 0; $i < count($array); $i++){
        for ($j = 0; $j < count($array); $j++){
            if($array[$j][1] > $array[$j - 1][1]){
                $swap1 = $array[$j][1];
                $swap2 = $array[$j][0];
                $array[$j][1] = $array[$j - 1][1];
                $array[$j][0] = $array[$j - 1][0];
                $array[$j - 1][1] = $swap1;
                $array[$j - 1][0] = $swap2;
            }
        }
    }
    return $array;
}

function buy_with(){
    $db = db_connect();
    $top_orders = select_order(top_orders);
    $orders_json = select_order(orders_ar15);
    for ($i = 0; $i < count($orders_json); $i++){
        $orders[$i] = json_decode($orders_json[$i][3]);
    }
    echo $orders[0] -> items -> item_kod;
    db_close($db);
}